package com.example.journalapp;


//use androidx instead
import android.content.Context;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import androidx.room.RoomDatabase;

@Entity(tableName = "journal_table")
public class Journal {
    @PrimaryKey(autoGenerate= true)
    @NonNull
    @ColumnInfo(name = "id")
    protected int id;
    private String title,content,published_on;
    public Journal(@NonNull int id)
    {this.id = id;
        this.title = title;
        this.content = content;
        this.published_on = published_on;
        }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }


    @ColumnInfo(name = "title")

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
    @ColumnInfo(name = "content")

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }
    @ColumnInfo(name = "published_on")

    public String getPublished_on() {
        return published_on;
    }

    public void setPublished_on(String published_on) {
        this.published_on = published_on;
    }
}
