package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalViewModel extends AndroidViewModel {
    private JournalRepository mRepository;
    LiveData<List<Journal>> AllJournals;

    public JournalViewModel (Application application) {
        super(application);
        mRepository = new JournalRepository(application);
        AllJournals = mRepository.getAllJournals();
    }
    LiveData<List<Journal>> getAllJournals() { return AllJournals; }

    public void insert(Journal journal) { mRepository.insert(journal); }
}
