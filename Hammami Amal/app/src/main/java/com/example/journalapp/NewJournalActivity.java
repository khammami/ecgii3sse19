package com.example.journalapp;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import androidx.appcompat.app.AppCompatActivity;

public class NewJournalActivity extends AppCompatActivity {
    public static final String EXTRA_REPLY =
            "com.example.android.roomwordssample.REPLY";

    private EditText mEditJournalView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_journal);
        mEditJournalView = findViewById(R.id.edit_title);

        final Button button = findViewById(R.id.button_D);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(mEditJournalView.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String journal = mEditJournalView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, journal);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }
}
