package com.example.journalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class JournalListAdapter extends RecyclerView.Adapter<JournalListAdapter.JournalViewHolder> {
    private final LayoutInflater mInflater;
    private List<Journal> mJournals; // Cached copy of words

    JournalListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public JournalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new JournalViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(JournalViewHolder holder, int position) {
        if ( mJournals != null) {
            Journal current =  mJournals.get(position);
            holder.journalItemView.setText(current.getTitle());
        } else {
            // Covers the case of data not being ready yet.
            holder.journalItemView.setText("No Word");
        }
    }

    void setJournals(List<Journal> journals){
        mJournals = journals;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if ( mJournals != null)
            return  mJournals.size();
        else return 0;
    }

    class JournalViewHolder extends RecyclerView.ViewHolder {
        private final TextView journalItemView;

        private JournalViewHolder(View itemView) {
            super(itemView);
            journalItemView = itemView.findViewById(R.id.textView);
        }
    }
}
