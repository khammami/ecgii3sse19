package com.example.noteapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao
public interface NoteDAO {
        @Insert
        void insert(Note Note);

        @Query("DELETE FROM note_table")
        void deleteAll();
    @Query("SELECT * from note_table ORDER BY id")
    LiveData<List<Note>> getAllIds();

}
