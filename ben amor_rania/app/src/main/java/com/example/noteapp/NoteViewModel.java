package com.example.noteapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteViewModel extends AndroidViewModel {


        private NoteRepository mRepository;

        private LiveData<List<Note>> mAllWords;

        public NoteViewModel (Application application) {
            super(application);
            mRepository = new NoteRepository(application);
            mAllWords = mRepository.getAllIds();
        }

    public static void deleteAll() {
    }

    public static void deleteWord(Note myWord) {
    }

    LiveData<List<Note>> getAllIds() { return mAllWords; }

        public void insert(Note Note) { mRepository.insert(Note); }
}
