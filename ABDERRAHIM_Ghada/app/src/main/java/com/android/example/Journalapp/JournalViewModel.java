/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.Journalapp;

import android.app.Application;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

/**
 * The WordViewModel provides the interface between the UI and the data layer of the app,
 * represented by the Repository
 */

public class JournalViewModel extends AndroidViewModel {

    private com.android.example.Journalapp.JournalRepository mRepository;

    private LiveData<List<com.android.example.Journalapp.Journal>> mAllWords;

    public JournalViewModel(Application application) {
        super(application);
        mRepository = new com.android.example.Journalapp.JournalRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<com.android.example.Journalapp.Journal>> getAllWords() {
        return mAllWords;
    }

    public void insert(Journal word) {
        mRepository.insert(word);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    public void deleteWord(com.android.example.Journalapp.Journal word) {
        mRepository.deleteWord(word);
    }

    public void update(Journal journal) {
        mRepository.update(journal);
    }
}
