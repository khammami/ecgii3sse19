/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.Journalapp;

import android.app.Application;
import androidx.lifecycle.LiveData;
import androidx.room.RoomDatabase;

import android.os.AsyncTask;

import java.util.List;

/**
 * This class holds the implementation code for the methods that interact with the database.
 * Using a repository allows us to group the implementation methods together,
 * and allows the WordViewModel to be a clean interface between the rest of the app
 * and the database.
 *
 * For insert, update and delete, and longer-running queries,
 * you must run the database interaction methods in the background.
 *
 * Typically, all you need to do to implement a database method
 * is to call it on the data access object (DAO), in the background if applicable.
 */

public class JournalRepository {

    private com.android.example.Journalapp.Journal mWordDao;
    private LiveData<List<com.android.example.Journalapp.Journal>> mAllWords;

    JournalRepository(Application application) {
        JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
        mWordDao = (Journal) db.wordDao();
        mAllWords = mWordDao.getAllWords;
    }

    LiveData<List<com.android.example.Journalapp.Journal>> getAllWords() {
        return mAllWords;
    }

    public void insert(com.android.example.Journalapp.Journal word) {
        new insertAsyncTask((JournalDao) mWordDao).execute(word);
    }

    public void update(com.android.example.Journalapp.Journal word)  {
        new updateWordAsyncTask((JournalDao) mWordDao).execute(word);
    }

    public void deleteAll()  {
        new deleteAllWordsAsyncTask((JournalDao) mWordDao).execute();
    }

    // Must run off main thread
    public void deleteWord(com.android.example.Journalapp.Journal word) {
        new deleteWordAsyncTask((JournalDao) mWordDao).execute(word);
    }

    // Static inner classes below here to run database interactions in the background.

    /**
     * Inserts a word into the database.
     */
    private static class insertAsyncTask extends AsyncTask<com.android.example.Journalapp.Journal, Void, Void> {

        private com.android.example.Journalapp.JournalDao mAsyncTaskDao;

        insertAsyncTask(com.android.example.Journalapp.JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final com.android.example.Journalapp.Journal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    /**
     * Deletes all words from the database (does not delete the table).
     */
    private static class deleteAllWordsAsyncTask extends AsyncTask<Void, Void, Void> {
        private com.android.example.Journalapp.JournalDao mAsyncTaskDao;

        deleteAllWordsAsyncTask(com.android.example.Journalapp.JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    /**
     *  Deletes a single word from the database.
     */
    private static class deleteWordAsyncTask extends AsyncTask<com.android.example.Journalapp.Journal, Void, Void> {
        private com.android.example.Journalapp.JournalDao mAsyncTaskDao;

        deleteWordAsyncTask(com.android.example.Journalapp.JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final com.android.example.Journalapp.Journal... params) {
            mAsyncTaskDao.deleteWord(params[0]);
            return null;
        }
    }

    /**
     *  Updates a word in the database.
     */
    private static class updateWordAsyncTask extends AsyncTask<com.android.example.Journalapp.Journal, Void, Void> {
        private com.android.example.Journalapp.JournalDao mAsyncTaskDao;

        updateWordAsyncTask(com.android.example.Journalapp.JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final com.android.example.Journalapp.Journal... params) {
            mAsyncTaskDao.update(params[0]);
            return null;
        }
    }
}
