/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.Journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.List;

/**
 * Entity class that represents a word in the database
 */

@Entity(tableName = "journale_table")
public class Journal {

    public LiveData<List<Journal>> getAllWords;
    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "titre")
    private String mJournale;

    public Journal(@NonNull String titre) {
        this.mJournale = titre;
    }

    /*
    * This constructor is annotated using @Ignore, because Room expects only
    * one constructor by default in an entity class.
    */

    @Ignore
    public Journal(int id, @NonNull String journale) {
        this.id = id;
        this.mJournale = journale;
    }

    public String getJournale() {
        return this.mJournale;
    }

    public int getId() {return id;}

    public void setId(int id) {
        this.id = id;
    }
}
