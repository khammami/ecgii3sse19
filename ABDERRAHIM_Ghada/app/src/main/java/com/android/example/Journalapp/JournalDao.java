/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.android.example.Journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;
import androidx.room.Update;

import java.util.List;



@Dao
public interface JournalDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(com.android.example.Journalapp.Journal journal);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Delete
    void deleteWord(com.android.example.Journalapp.Journal journal);

    @Query("SELECT * from journal_table LIMIT 1")
    com.android.example.Journalapp.Journal[] getAnyWord();

    @Query("SELECT * from journal_table ORDER BY titre ASC")
    LiveData<List<com.android.example.Journalapp.Journal>> getAllWords();

    @Update
    void update(com.android.example.Journalapp.Journal... word);
}
