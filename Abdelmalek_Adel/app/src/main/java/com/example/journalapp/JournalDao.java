package com.example.journalapp;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;


@Dao
public abstract class JournalDao {

    @Insert
    void insert(journals jounal);

    @Query("DELETE FROM journal_table")
    void deleteAll();



    @Query("SELECT * from journal_table ORDER BY word ASC")

    LiveData<List<Word>> getAllWords();
}
