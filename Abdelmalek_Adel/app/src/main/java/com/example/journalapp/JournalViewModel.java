package com.example.journalapp;

import android.app.Application;

public class JournalViewModel {
    private JournalRepository mRepository;
    private LiveData<List<Word>> mAllWords;

    public JournalViewModel (Application application) {
        super(application);
        mRepository = new JournalRepository(application);
        mAllWords = mRepository.getAllWords(); }
    LiveData<List<Word>> getAllWords() { return mAllWords; }
    public void insert(Word word) { mRepository.insert(word); }
}