package com.example.journalapp;

import android.content.Context;

import androidx.room.Room;
import androidx.room.RoomDatabase;

public abstract class JournalRoomDatabase extends RoomDatabase {
    public abstract JournalDao journalDao();

    private static JournalRoomDatabase INSTANCE;

    static JournalRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (JournalRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            JournalRoomDatabase.class, "journal_table")
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }}