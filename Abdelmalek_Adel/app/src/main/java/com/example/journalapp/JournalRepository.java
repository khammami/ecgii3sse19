package com.example.journalapp;

import android.app.Application;

public class JournalRepository {

    private JournalDao mJournalDao;
    private LiveData<List<Word>> mAllWords;

    JournalRepository(Application application) {
        JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
        mJournalDao = db.journalDao();
        mAllWords = mJournalDao.getAllWords();
    }

    LiveData<List<Word>> getAllWords() {
        return mAllWords;
    }

    public void insert(Journal journal) {
        new insertAsyncTask(mJournalDao).execute(journal);
    }

    private static class insertAsyncTask extends AsyncTask<Word, Void, Void> {

        private JournalDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Word... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
