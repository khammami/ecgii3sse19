package com.example.journalapp;


import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.Ignore;
import androidx.room.PrimaryKey;

@Entity(tableName = "journal_table")
public class journals<date> {


    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int mID;

    public void setmID(int mID) {
        this.mID = mID;
    }

    public journals(int id)
    {this.mID = id;}
    public int getmJornals(){return this.mID;}


    @Ignore
    @NonNull
    @ColumnInfo(name = "title")

    private String mtitle;

    public journals(String title)
    {this.mtitle = title;}

    public String getMtitle() {return mtitle;}
    public void setMtitle(String mtitle) {this.mtitle = mtitle;}

    @Ignore
    @NonNull
    @ColumnInfo(name = "content")
    private String mContent;

    {this.mContent = mContent;}
    @NonNull
    public String getmContent() {
        return mContent;
    }

    public void setmContent(@NonNull String mContent) {
        this.mContent = mContent;
    }

    @Ignore
    @ColumnInfo(name = "Published_on")
    private date mpublished;

    {this.mpublished= mpublished;}

    public date getMpublished() {
        return mpublished;
    }

    public void setMpublished(date mpublished) {
        this.mpublished = mpublished;
    }













}

