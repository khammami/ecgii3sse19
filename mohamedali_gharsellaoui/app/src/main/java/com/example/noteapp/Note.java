package com.example.noteapp;

import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;
import java.util.Date;

@Entity(tableName = "note_table")
public class Note {

    @PrimaryKey (autoGenerate = true)
    private int id;
    private String title;
    private String content;
    private Date published_on ;
    @NonNull

    private String mNote;

    public Note(@NonNull String title ,String content, int id,Date published_on )
    {this.id = id;
    this.title=title;
    this.content=content;
    this.published_on=published_on;
    }

    @ColumnInfo(name = "id")
    private int mid;
    public int getid(){return this.id;}

    @ColumnInfo(name = "title")
    private String mtitle;
    public String gettitle(){return this.title;}

    @ColumnInfo(name = "content")
    private String mcontent;
    public String getcontent(){return this.mcontent;}

    @ColumnInfo(name = "published_on")
    private Date mpublished_on;
    public Date getdate(){return this.mpublished_on;}


}
