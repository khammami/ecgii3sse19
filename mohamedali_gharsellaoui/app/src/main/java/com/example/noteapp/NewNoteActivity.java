package com.example.noteapp;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class NewNoteActivity extends AppCompatActivity {
    public static final String EXTRA_REPLY =
            "com.example.android.roomwordssample.REPLY";
    public static final int NEW_NOTE_ACTIVITY_REQUEST_CODE = 1;
    private EditText mEditidView;
    private EditText mEdittitleView;
    private EditText mEditcontentView;
    private EditText mEditdateView;
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_new_note);
        mEditidView = findViewById(R.id.edit_id);
        mEdittitleView = findViewById(R.id.edit_title);
        mEditcontentView = findViewById(R.id.edit_content);
        mEditdateView = findViewById(R.id.edit_date);


        final Button button = findViewById(R.id.button_save);
        button.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {

                Intent replyIntent = new Intent();
                if ( ( TextUtils.isEmpty(mEditidView.getText() ))
                        || ( TextUtils.isEmpty(mEditcontentView.getText())) ||
                            (TextUtils.isEmpty(mEdittitleView.getText()) ))
                {
                    setResult(RESULT_CANCELED, replyIntent);
                }   else {

                    String title = mEdittitleView.getText().toString();
                    String content = mEditcontentView.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, title);
                    setResult(RESULT_OK, replyIntent);
                    Intent intent = new Intent(MainActivity.this, NewNoteActivity.class);
                    startActivityForResult(intent, NEW_NOTE_ACTIVITY_REQUEST_CODE);
                }
                finish();
            }
        });
    }
}
