package com.example.noteapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;
import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter {

    private final LayoutInflater mInflater;
    private List<Note> mNotes;

   NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new Note"ViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull RecyclerView.ViewHolder holder, int position) {
        if (mNotes != null) {
            Note current = mNotes.get(position);
            holder.TextItemView.setText(current.getid());
            holder.noteItemView.setText(current.gettitle());
            holder.noteItemView.setText(current.getcontent());
            holder.noteItemView.setText((CharSequence) current.getdate());
        } else {
            holder.noteItemView.setText("No note");
        }
    }



    void setNotes(List<Note> notes){
        mNotes = notes;
        notifyDataSetChanged();
    }


    @Override
    public int getItemCount() {
        if (mNotes != null)
            return mNotes.size();
        else return 0;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private final TextView noteItemView;

        private NoteViewHolder(View itemView) {
            super(itemView);
            noteItemView = itemView.findViewById(R.id.textView);
        }
    }
}
