/*
 * Copyright (C) 2018 Google Inc.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.example.journalapp.examen;
import androidx.annotation.NonNull;

@Entity(tableName = "journal_table")
public class Word {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @NonNull
    @ColumnInfo(name = "title")
    private String mWord;
    @NonNull
    @ColumnInfo(name = "content")
    private String mWord1;
    @NonNull
    @ColumnInfo(name = "published_on")
    private date  mWord2;

    public Word(@NonNull String word,@NonNull String word1,@NonNull date word2) {
        this.mWord = word;
        this.mWord1 = word1;
        this.mWord2 = word2;
    }

    @Ignore
    public Word(int id, @NonNull String word) {
        this.id = id;
        this.mWord = word;
    }


    public String getWord() {
        return this.mWord;
    }
    public String getWord1() {
        return this.mWord1;
    }
    public  date getDate() {
        return this.mWord2;
    }

    public int getId() {return id;}

    public void setId(int id) {
        this.id = id;
    }
}
