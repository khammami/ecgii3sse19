package com.example.myapplication;

public class noteRepository {
    private DAO dao;
    private LiveData<List<title>> mAllWords;

    noteRepository(Application application) {
        noteRoomDatabase db = noteRoomDatabase.getDatabase(application);
        dao = db.wordDao();
        mAllWords = dao.getAllWords();
    }

    LiveData<List<title>> getAllWords() {
        return mAllWords;
    }

    public void insert (String title,String content,String published_on) {
        new insertAsyncTask(dao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<title, Void, Void> {

        private dao mAsyncTaskDao;

        insertAsyncTask(WordDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final title... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
