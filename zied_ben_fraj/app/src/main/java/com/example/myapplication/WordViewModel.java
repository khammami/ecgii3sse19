package com.example.myapplication;

import androidx.lifecycle.AndroidViewModel;

public class WordViewModel extends AndroidViewModel {
    private noteRepository mRepository;

    private LiveData<List<title>> mAllWords;

    public WordViewModel (Application application) {
        super(application);
        mRepository = new noteRepository(application);
        mAllWords = mRepository.getAllWords();
    }

    LiveData<List<title>> getAllWords() { return mAllWords; }

    public void insert(DAO dao) { mRepository.insert(title); }
}
