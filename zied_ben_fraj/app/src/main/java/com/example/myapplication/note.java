package com.example.myapplication;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(tableName = "table_note")
public class note {

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "id")
    private int id;
    private String title;
    private String content;
    private String published_on;

    public note(int id, String title, String content, String published_on) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.published_on = published_on;
    }

    public void setId(int id) {
        this.id = id;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublished_on(String published_on) {
        this.published_on = published_on;
    }



    public int getId() {
        return id;
    }

    public String getTitle() {
        return title;
    }

    public String getContent() {
        return content;
    }

    public String getPublished_on() {
        return published_on;
    }


}
