package com.example.myapplication;

import androidx.room.Insert;
import androidx.room.Query;
import androidx.room.Update;

public interface DAO {
    //insert
    @Insert
    void insert(String title,String content,String published_on);
    //get all
    @Query("SELECT * from table_note ORDER BY id ASC")
    LiveData<List<title>> getAllWords();

    //delete
    @Query("DELETE FROM table_note where id=?")
    void delete(id);

    //delete all
    @Query("DELETE  FROM table_note")
    void deleteAll();
    @Update
    void update(int id,String title,String content,String published_on);
}
