package com.example.exam;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

public class NoteListAdapter  extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder> {
        private final LayoutInflater mInflater;
        private List<notrEntity> mNote;

    NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.content_main, parent, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull NoteViewHolder holder, int position) {

    }


    void setNote(List<notrEntity> note){
        mNote = note;
        notifyDataSetChanged();
    }

    @Override
    public int getItemCount() {
        if (mNote != null)
            return mNote.size();
        else return 0;
    }

    class NoteViewHolder extends RecyclerView.ViewHolder {
        private final EditText Title;
        private final EditText contenet;



        private NoteViewHolder(View itemView) {
            super(itemView);
            Title = itemView.findViewById(R.id.titre);
            contenet = itemView.findViewById(R.id.cont);
        }
    }

}
