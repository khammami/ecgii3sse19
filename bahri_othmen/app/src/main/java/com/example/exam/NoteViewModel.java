package com.example.exam;

import android.app.Application;

import java.util.List;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;

public class NoteViewModel extends AndroidViewModel {

    private NoteRepository mRepository;

    private List<notrEntity> mAllNote;

    public NoteViewModel(@NonNull Application application) {
        super(application);
        mRepository = new NoteRepository(application);
        mAllNote = mRepository.getAllNote();
    }

    List<notrEntity> getAllWords() { return mAllNote; }

    public void insert(notrEntity note) { mRepository.insert(note); }
}
