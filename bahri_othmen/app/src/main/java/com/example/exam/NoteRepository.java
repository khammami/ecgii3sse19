package com.example.exam;

import android.app.Application;
import android.os.AsyncTask;

import java.util.List;

public class NoteRepository {

    private noteDao mWordDao;
    private List<notrEntity> mAllnote;

    NoteRepository(Application application) {
        NoteRoomDatabase db = NoteRoomDatabase.getDatabase(application);
        mWordDao = db.noteDao();
        mAllnote = mWordDao.getAllnotes();
    }

    List<notrEntity> getAllNote() {
        return mAllnote;
    }

    public void insert (notrEntity note) {
        new insertAsyncTask(mWordDao).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask<notrEntity, Void, Void> {

        private noteDao mAsyncTaskDao;

        insertAsyncTask(noteDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final notrEntity... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }


}
