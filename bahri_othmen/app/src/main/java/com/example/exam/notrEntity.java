package com.example.exam;

import java.util.Date;

//use androidx instead
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

@Entity(tableName = "note_table")
public class notrEntity {


    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private int id;
    @NonNull
    private String title;
    @NonNull
    private String content;
    @NonNull
    private Date d;

    public int getId() {
        return id;
    }



    public void setId(int id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getD() {
        return d;
    }

    public void setD(Date d) {
        this.d = d;
    }

    public notrEntity(String title) {
        this.title = title;
        this.content = content;
        this.d = d;
    }

    public notrEntity(int id, @NonNull String title, @NonNull String content, @NonNull Date d) {
        this.id = id;
        this.title = title;
        this.content = content;
        this.d = d;
    }
}
