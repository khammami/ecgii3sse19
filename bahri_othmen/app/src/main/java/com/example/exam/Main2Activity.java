package com.example.exam;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.text.TextUtils;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class Main2Activity extends AppCompatActivity {

    public static final String EXTRA_REPLY =
            "com.example.android.roomwordssample.REPLY";

    private EditText title;
    private EditText cont;
    public static final int NEW_WORD_ACTIVITY_REQUEST_CODE = 1;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate( savedInstanceState );
        setContentView( R.layout.activity_main2 );

       title = findViewById(R.id.titre);
        cont = findViewById(R.id.cont);
        final Button button = findViewById(R.id.button_save);

        button.setOnClickListener(new View.OnClickListener(){
            public void onClick(View view) {
                Intent replyIntent = new Intent();
                if (TextUtils.isEmpty(title.getText()) && !TextUtils.isEmpty( cont.getText())) {
                    setResult(RESULT_CANCELED, replyIntent);
                } else {
                    String word = title.getText().toString();
                    String n = cont.getText().toString();
                    replyIntent.putExtra(EXTRA_REPLY, word);
                    replyIntent.putExtra(EXTRA_REPLY, n);
                    setResult(RESULT_OK, replyIntent);
                }
                finish();
            }
        });
    }

    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if (requestCode == NEW_WORD_ACTIVITY_REQUEST_CODE && resultCode == RESULT_OK) {
            notrEntity word = new notrEntity(data.getStringExtra(Main2Activity.EXTRA_REPLY));
            NoteViewModel mWordViewModel;
            mWordViewModel.insert(word);
        } else {
            Toast.makeText( this, "", Toast.LENGTH_SHORT ).show();.makeText(
                    getApplicationContext(),
                    R.string.empty_not_saved,
                    Toast.LENGTH_LONG).show();
        }
    }

    }

