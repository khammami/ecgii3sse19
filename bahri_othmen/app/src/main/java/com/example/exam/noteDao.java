package com.example.exam;


import java.util.List;

import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

@Dao
public interface noteDao {

    @Insert
    void insert(notrEntity e);


    @Query("DELETE FROM note_table")
    void deleteAll();

    @Query("SELECT * from note_table ")
    List<notrEntity> getAllnotes();


}
