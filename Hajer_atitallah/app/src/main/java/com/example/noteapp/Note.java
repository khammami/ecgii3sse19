package com.example.noteapp;


import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;
import androidx.annotation.NonNull;

import java.util.Date;

@Entity(tableName = "note_table")
public class Note {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")
    private int mid;
    @ColumnInfo(name = "title")
    private String mtitle;
    @ColumnInfo(name = "content")
    private String mcontent;
    @ColumnInfo(name = "published_on")
    private Date mpublished_on;


    public Note(@NonNull int id, String title, String mtitle, String mcontent, Date published_on) {this.mid = id;
        this.mtitle = mtitle;
        this.mcontent = mcontent;
        this.mpublished_on = mpublished_on;
    }

    public int getNote(){return this.mid;}


}

