package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

@Dao

public interface JournalDao {
    @Insert
    void insert(Journal journal);
    @Query("DELETE FROM journa_table")
    void deleteAll();


    @Query("SELECT id from journal_table ORDER BY id ASC")
    LiveData<List<Journal>> getAllID();

    @Query("SELECT title from journal_table ORDER BY title ASC")
    LiveData<List<Journal>> getAllTitle();
    @Query("SELECT content from journal_table ORDER BY content ASC")
    LiveData<List<Journal>> getAllContent();

    @Query("SELECT oublished_on from journal_table ORDER BY published_on ASC")
    LiveData<List<Journal>> getAllDate();
}
