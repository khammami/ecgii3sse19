package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;
import java.util.StringTokenizer;

public class JournalRepository {

    private JournalDao mJournalDao;
    private LiveData<List<Journal>>  mId,mtitle,mcontent,mdate;

    JournalRepository(Application application) {
        JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
        mJournalDao = db.IDDao();
        mId=mJournalDao.getAllID();

        mJournalDao = db.TitleDao();
        mtitle=mJournalDao.getAllTitle();

        mJournalDao = db.ContentDao();
        mcontent=mJournalDao.getAllContent()
        mJournalDao = db.IDDao();
        mdate=mJournalDao.getAllDate();
    }

    LiveData<List<Journal>> getTitle() {
        return mtitle ;
    }
    LiveData<List<Journal>> getContent() {
        return mcontent ;
    }
    LiveData<List<Journal>> getDate() {
        return mdate ;
    }


    public void insert (String title ) {
        new insertAsyncTask(mJournalDao).execute(word);
    }

    private static class insertAsyncTask extends AsyncTask<Word, Void, Void> {

        private WordDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Word... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}
