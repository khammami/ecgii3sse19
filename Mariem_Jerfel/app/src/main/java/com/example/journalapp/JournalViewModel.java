package com.example.journalapp;

import android.app.Application;

import androidx.annotation.NonNull;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;


import java.util.List;

public class JournalViewModel extends AndroidViewModel {

    private JournalRepository mRepository;
   private LiveData<List<Journal>> mallJournals;
    public JournalViewModel(@NonNull Application application) {
        super(application);
        mRepository = new JournalRepository(application);
        LiveData<List<Journal>> title = mRepository.getTitle();
        LiveData<List<Journal>> content = mRepository.getContent();
        LiveData<List<Journal>> date =mRepository.getDate();

        String getTitle() { return title; }

        public void insert(Journal) { mRepository.insert(title); }

    }
}
