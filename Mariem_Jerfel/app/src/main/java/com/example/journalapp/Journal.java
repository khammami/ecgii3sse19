package com.example.journalapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.PrimaryKey;

import java.util.Date;

public class Journal {

    @PrimaryKey
    @NonNull
    @ColumnInfo(name = "id")
    private int mID;

    public Journal(@NonNull int ID) {this. mID = ID;}

    public int getmID(){return this.mID;}

    @NonNull
    @ColumnInfo(name = "title")
    private String mtitle;

    public Journal(@NonNull String title) {this.mtitle = title;}
    //public String getMtitlel{return this.mtitle;}
    public String getMtitle{
        return this.mtitle;
    }


    @NonNull
    @ColumnInfo(name = "published_on")
    private String mdate;

    public Journal(@NonNull Date date) {this.mdate = mdate;}
    public String getMdate(){return this.mdate;}
}
