package com.example.journalapp;



import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

@Entity(tableName = "journal_table")
public class Journal {

    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "Jid")
    private int id;
    private String tiltle;
    private String content;
    private Date published_on;

    public int getId() {
        return id;
    }

    public String getTiltle() {
        return tiltle;
    }

    public String getContent() {
        return content;
    }

    public Date getPublished_on() {
        return published_on;
    }

    public void setTiltle(String tiltle) {
        this.tiltle = tiltle;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public void setPublished_on(Date published_on) {
        this.published_on = published_on;
    }

    public Journal(int id, String tiltle, String content, Date published_on) {
        this.id = id;
        this.tiltle = tiltle;
        this.content = content;
        this.published_on = published_on;
    }

    public Journal(int id) {
        this.id = id;
    }

    public Journal(String tiltle, String content, Date published_on) {
        this.tiltle = tiltle;
        this.content = content;
        this.published_on = published_on;
    }
}
