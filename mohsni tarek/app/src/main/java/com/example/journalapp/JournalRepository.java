package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalRepository {

    private JournalDao mJournalDao;
    private LiveData<List<id>> mAllJournal;

    JournalRepository(Application application) {
       JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
        mJournalDao = (JournalDao) db.JournalDao();
        mAllJournal = mJournalDao.getAllJournal();
    }

    LiveData<List<id>> getAllWords() {
        return mAllJournal;
    }

    public void insert(Journal j) {
        new insertAsyncTask(mJournalDao).execute(j);
    }

    public void deleteAll() {
        new deleteAllJournalsAsyncTask(mJournalDao).execute();
    }

    // Need to run off main thread
    public void deleteJournal(Journal j) {
        new deleteJournalAsyncTask(mJournalDao).execute(j);
    }

    // Static inner classes below here to run database interactions
    // in the background.

    /**
     * Insert a word into the database.
     */
    private static class insertAsyncTask extends AsyncTask<Journal, Void, Void> {

        private JournalDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Journal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }

    /**
     * Delete all words from the database (does not delete the table)
     */
    private static class deleteAllJournalsAsyncTask extends AsyncTask<Void, Void, Void> {
        private JournalDao mAsyncTaskDao;

        deleteAllJournalsAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }

    /**
     *  Delete a single word from the database.
     */
    private static class deleteJournalAsyncTask extends AsyncTask<Journal, Void, Void> {
        private JournalDao mAsyncTaskDao;

        deleteJournalAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Journal... params) {
            mAsyncTaskDao.deleteJournal(params[0]);
            return null;
        }
    }
}

