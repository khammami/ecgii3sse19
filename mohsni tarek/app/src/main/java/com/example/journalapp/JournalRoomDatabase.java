package com.example.journalapp;
import androidx.sqlite.db.SupportSQLiteDatabase;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import android.content.Context;

import androidx.annotation.NonNull;
@Database(entities = {Journal.class}, version = 1, exportSchema = false)

public class JournalRoomDatabase extends RoomDatabase {

    public abstract JournalDao JournalDao();

    private static JournalRoomDatabase INSTANCE;

    static JournalRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (JournalRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            JournalRoomDatabase.class, "journal_table_database")
                            // Wipes and rebuilds instead of migrating
                            // if no Migration object.
                            // Migration is not part of this practical.
                            .fallbackToDestructiveMigration()
                            .build();
                }
            }
        }
        return INSTANCE;
    }
    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                @Override
                public void onOpen (@NonNull SupportSQLiteDatabase db){
                    super.onOpen(db);
                    new PopulateDbAsync(INSTANCE).execute();
                }
            };


    public void JournalDao() {
    }
    @Override
    protected Void doInBackground(final Void... params) {
        // If we have no words, then create the initial list of words
        if (mDao.getAnyid().length < 1) {
            for (int i = 0; i <= Jounals.length - 1; i++) {
                Journal j = new Journal(Jounals[i]);
                mDao.insert(j);
            }
        }
        return null;
    }
}
