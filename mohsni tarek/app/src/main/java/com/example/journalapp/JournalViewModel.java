package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalViewModel extends AndroidViewModel {

    private JournalRepository mRepository;

    private LiveData<List<Journal>> mAllJournal;

    public JournalViewModel(Application application) {
        super(application);
        mRepository = new JournalRepository(application);
        mAllJournal = mRepository.getAllJournal();
    }

    LiveData<List<Journal>> getAllJournal() {
        return mAllJournal;
    }

    public void insert(Journal j) {
        mRepository.insert(j);
    }

    public void deleteAll() {
        mRepository.deleteAll();
    }

    public void deletejournal(Journal j) {
        mRepository.deleteJournal(j);
    }
}

