package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Delete;
import androidx.room.Insert;
import androidx.room.OnConflictStrategy;
import androidx.room.Query;

import java.util.List;

@Dao
public interface JournalDao {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insert(Journal id);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Delete
    void deleteJournal(Journal id);

    @Query("SELECT * from journal_table LIMIT 1")
    Journal[] getAnyid();

    @Query("SELECT * from journal_table ORDER BY Jid ASC")
    LiveData<List<id>> getAllJournal();

}
