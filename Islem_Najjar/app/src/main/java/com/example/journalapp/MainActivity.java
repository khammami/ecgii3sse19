package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.lifecycle.AndroidViewModel;
import androidx.lifecycle.LiveData;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;


import com.google.android.material.floatingactionbutton.FloatingActionButton;
import com.google.android.material.snackbar.Snackbar;

import java.util.Date;
import java.util.List;


public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
    @Entity(tableName = "journal_table")

    public class id {
        @PrimaryKey(autoGenerate = true)
        @NonNull
        @ColumnInfo(name = "id")
        private int mId;

        public id(@NonNull int id) {this.mId = id;}

        public int getId(){return this.mId;}
        public void setid(int id){this.mId=id;}
    }
    public class title {

        @ColumnInfo(name = "title")

        private String mtitle;

        public title( String title) {this.mtitle = title;}

        public String gettitle(){return this.mtitle;}
        public void settitle(String tit){this.mtitle=tit;}

    }
    public class content {
        @ColumnInfo(name = "content")


        private String mcontent;

        public content( String content) {this.mcontent = content;}

        public String gettitle(){return this.mcontent;}
        public void setcontent(String cont){this.mcontent=cont;}

    }
    public class date {

        @ColumnInfo(name = "published_on")

        private Date mpub;

        public date( Date published_on) {this.mpub = published_on;}

        public Date getdate(){return this.mpub;}
        public void setdate(Date pub){this.mpub=pub;}
        }

        public class Journal {

        private id idJ;
        private title titleJ;
        private content contentJ;
        private date published_inJ;

        public Journal(id mid,title mtitle,content mcontent,date mdate){
            this.idJ=mid;
            this.titleJ=mtitle;

            this.contentJ=mcontent;
            this.published_inJ=mdate;

        }

        }
    public static class JournalRepository {

        private idDao mJournalDao;
        private LiveData<List<Journal>> mAllJournals;

        JournalRepository(Application application) {
            JournalRoomDatabase db = JournalRoomDatabase.getDatabase(application);
            mJournalDao = db.idDao();
            mAllJournals = mJournalDao.getAllJournals();
        }



        public void insert (Journal journal) {
            new insertAsyncTask(mJournalDao).execute(journal);
        }

        private static class insertAsyncTask extends AsyncTask<Journal, Void, Void> {

            private idDao mAsyncTaskDao;

            insertAsyncTask(idDao dao) {
                mAsyncTaskDao = dao;
            }

            @Override
            protected Void doInBackground(final Journal... params) {
                mAsyncTaskDao.insert(params[0]);
                mAsyncTaskDao.insert(params[1]);
                mAsyncTaskDao.insert(params[2]);
                mAsyncTaskDao.insert(params[3]);
                return null;
            }
        }
    }



    public class JournalViewModel extends AndroidViewModel {

        private JournalRepository mRepository;

        private LiveData<List<Journal>> mAllJournals;

        public JournalViewModel (Application application) {
            super(application);
            mRepository = new JournalRepository(application);

        }

        LiveData<List<Journal>> getAllJournals() { return mAllJournals; }

        public void insert(Journal journal) { mRepository.insert(journal); }
    }
    }





