package com.example.journalapp;


import androidx.lifecycle.LiveData;
import androidx.room.Dao;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.Date;
import java.util.List;

@Dao
public interface idDao {

@Insert
        void insert(MainActivity.Journal jornal);

        @Query("DELETE FROM journal_table")
        void deleteAll();

        @Query("SELECT * from journal_table ORDER BY id ASC")
        LiveData<List<MainActivity.Journal>> getAllJournals();


}
