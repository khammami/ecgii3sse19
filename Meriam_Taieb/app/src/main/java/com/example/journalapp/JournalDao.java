package com.example.journalapp;

import androidx.lifecycle.LiveData;
import androidx.room.Insert;
import androidx.room.Query;

import java.util.List;

public interface JournalDao {
    @Insert
    void insert( Journal journal);

    @Query("DELETE FROM journal_table")
    void deleteAll();

    @Query("SELECT * from journal_table ")

    LiveData<List<Journal>> getAllJournal();
}
