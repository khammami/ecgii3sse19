package com.example.journalapp;

import androidx.annotation.NonNull;
import androidx.room.ColumnInfo;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import java.sql.Date;

@Entity(tableName = "journal_table")
public class Journal {
    @PrimaryKey(autoGenerate = true)
    @NonNull
    @ColumnInfo(name = "id")

    private int id;
    public int getId() {
        return id;
    }
    public Journal(@NonNull int id, String title, String content, Date published_on)
    {this.id = id;
    this.title=title;
    this.content=content;
    this.published_on=published_on;
    }

    private String title;
    @ColumnInfo(name = "title")
    public String getTitle() {
        return title;
    }

    private String content;
    @ColumnInfo(name = "content")
    public String getContent() {
        return content;
    }
    private Date published_on ;
    @ColumnInfo(name = "published_on")
    public Date getPublished_on() {
        return published_on;
    }


}
