package com.example.journalapp;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalRepository {
    private JournalDao mjournalDao;
    private LiveData<List<Journal>> AllJournal;

    JournalRepository(Application application) {
        JournalRoomDatabase db =JournalRoomDatabase .getDatabase(application);
        mjournalDao = db.JournalDao();
        AllJournal = mjournalDao.getAllJournal();
    }

    LiveData<List<Journal>> getAllJournal() {
        return AllJournal;
    }

    public void insert (Journal journal ) {
        new insertAsyncTask(mjournalDao).execute(journal);
    }

    private static class insertAsyncTask extends AsyncTask<Journal, Void, Void> {

        private JournalDao mAsyncTaskDao;

        insertAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Journal... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
    private static class deleteAllWordsAsyncTask extends AsyncTask<Void, Void, Void> {
        private JournalDao mAsyncTaskDao;

        deleteAllWordsAsyncTask(JournalDao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(Void... voids) {
            mAsyncTaskDao.deleteAll();
            return null;
        }
    }
    public void deleteAll()  {
        new deleteAllWordsAsyncTask(mjournalDao).execute();
    }
}
