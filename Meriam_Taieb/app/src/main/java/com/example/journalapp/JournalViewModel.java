package com.example.journalapp;

import android.app.Application;

import androidx.lifecycle.LiveData;

import java.util.List;

public class JournalViewModel {
    private JournalRepository mRepository;

    private LiveData<List<Journal>> AllJournal;

    public JournalViewModel (Application application)

    { //super(application);
        mRepository = new JournalRepository(application);
        AllJournal = mRepository.getAllJournal();
    }

    LiveData<List<Journal>> getAllJournal() { return AllJournal; }

    public void insert(Journal journal ) { mRepository.insert(journal); }
    public void deleteAll() {mRepository.deleteAll();}
}
