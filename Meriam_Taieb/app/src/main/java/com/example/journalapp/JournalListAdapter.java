package com.example.journalapp;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class JournalListAdapter extends RecyclerView.Adapter<JournalListAdapter.JournalViewHolder>
    {
    private final LayoutInflater mInflater;
    private List<Journal> mJournal; // Cached copy of words

    JournalListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public JournalViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.recyclerview_item, parent, false);
        return new JournalViewHolder (itemView);
    }

    @Override
    public void onBindViewHolder(JournalViewHolder holder, int position) {
        if (mJournal != null) {
            Journal current = mJournal.get(position);
            holder. journalItemView.setText(current.getContent());
        } else {
            // Covers the case of data not being ready yet.
            holder. journalItemView.setText("No Journal");
        }
    }

    void setWords(List<Journal> journal){
        mJournal = journal;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mJournal != null)
            return mJournal.size();
        else return 0;
    }

    class JournalViewHolder extends RecyclerView.ViewHolder {
        private final TextView journalItemView;

        private JournalViewHolder(View itemView) {
            super(itemView);
            journalItemView = itemView.findViewById(R.id.textView);
        }
    }
}
