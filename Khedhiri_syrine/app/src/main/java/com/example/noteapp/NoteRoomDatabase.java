package com.example.noteapp;

import android.content.Context;
import android.os.AsyncTask;

import androidx.annotation.NonNull;
import androidx.room.Database;
import androidx.room.Room;
import androidx.room.RoomDatabase;
import androidx.room.TypeConverter;
import androidx.room.TypeConverters;
import androidx.sqlite.db.SupportSQLiteDatabase;

import java.util.Date;

@Database(entities = {Note.class}, version = 1, exportSchema = false)
public abstract class NoteRoomDatabase extends RoomDatabase {

    public abstract NoteDao noteDao();
    private static NoteRoomDatabase INSTANCE;

    static NoteRoomDatabase getDatabase(final Context context) {
        if (INSTANCE == null) {
            synchronized (NoteRoomDatabase.class) {
                if (INSTANCE == null) {
                    INSTANCE = Room.databaseBuilder(context.getApplicationContext(),
                            NoteRoomDatabase.class, "note_database")

                            .fallbackToDestructiveMigration()
                            .build();

                }
        }
        return INSTANCE;
    }

    private static RoomDatabase.Callback sRoomDatabaseCallback =
            new RoomDatabase.Callback(){

                class PopulateDbAsync extends AsyncTask<Void, Void, Void> {

                    private final NoteDao mDao;
                    String[] notes = {"Note1", "Note2", "Note3"};

                    PopulateDbAsync (NoteRoomDatabase db) {
                        mDao = db.noteDao();
                    }

                    @Override
                    protected Void doInBackground (final Void... params) {
                        // Start the app with a clean database every time.
                        // Not needed if you only populate the database
                        // when it is first created
                        mDao.deleteAll();

                        for (int i = 0; i <= notes.length - 1; i++) {
                            Note note = new Note(notes[i]);
                            mDao.insert(note);
                        }
                        return null;


                    }
                }};}
