package com.example.myapplication;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import java.util.List;

public class NoteListAdapter extends RecyclerView.Adapter<NoteListAdapter.NoteViewHolder>{

    private final LayoutInflater mInflater;
    private List<Note> mWords;
   NoteListAdapter(Context context) { mInflater = LayoutInflater.from(context); }

    @Override
    public NoteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = mInflater.inflate(R.layout.content_main, parent, false);
        return new NoteViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(NoteViewHolder holder, int position) {
        if (mWords!= null) {
            Note current = mWords.get(position);
            holder.title.setText(current.getTitle());

            holder.content.setText(current.getContent());
        } else {
            // Covers the case of data not being ready yet.
            holder.title.setText("AJOUTER UN NOTE");

        }
    }

    void setWords(List<Note> words){
        mWords = words;
        notifyDataSetChanged();
    }

    // getItemCount() is called many times, and when it is first called,
    // mWords has not been updated (means initially, it's null, and we can't return null).
    @Override
    public int getItemCount() {
        if (mWords != null)
            return mWords.size();
        else return 0;
    }

class NoteViewHolder extends RecyclerView.ViewHolder {
    private final TextView title;
    private final TextView published_on;
    private  final TextView content;


    private NoteViewHolder(View itemView) {
        super(itemView);
        title= itemView.findViewById(R.id.textView);
        published_on= itemView.findViewById(R.id.textView2);
        content= itemView.findViewById(R.id.textView3);
    }
}
}