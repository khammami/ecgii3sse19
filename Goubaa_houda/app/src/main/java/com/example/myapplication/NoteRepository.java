package com.example.myapplication;

import android.app.Application;
import android.os.AsyncTask;

import androidx.lifecycle.LiveData;

import java.util.List;

public class NoteRepository {
    private Notedao notedao;
    private LiveData<List<Note>> mAllWords;

    NoteRepository(Application application) {
       NoteRoomDatabase db =NoteRoomDatabase.getDatabase(application);
        notedao = db.noteDao();
        mAllWords =notedao.getAllWords();
    }

    LiveData<List<Note>> getAllWords() {
        return mAllWords;
    }

    public void insert (Note note) {
        new insertAsyncTask(notedao).execute(note);
    }

    private static class insertAsyncTask extends AsyncTask< Note, Void, Void> {

        private Notedao mAsyncTaskDao;

        insertAsyncTask(Notedao dao) {
            mAsyncTaskDao = dao;
        }

        @Override
        protected Void doInBackground(final Note... params) {
            mAsyncTaskDao.insert(params[0]);
            return null;
        }
    }
}